package com.example.objectmapper.model.mail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MailGroups {
  @JsonProperty("name")
  private String name;
  @JsonProperty("hosts")
  List<Hosts> hosts;
  @JsonProperty("mailTypes")
  private List<String> mailTypes;

}
