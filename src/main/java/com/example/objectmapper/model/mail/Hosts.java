package com.example.objectmapper.model.mail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Hosts {
  @JsonProperty("host")
  private String host;
  @JsonProperty("weight")
  private int weight;

}
