package com.example.objectmapper;

import com.example.objectmapper.model.building.Building;
import com.example.objectmapper.model.mail.MailObject;
import com.example.objectmapper.model.news.News;
import com.example.objectmapper.model.user.User;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

import static com.example.objectmapper.Jsons.*;

@Slf4j
public class Deserialize {
  static ObjectMapper objectMapper = new ObjectMapper();

  @SneakyThrows
  public static void main(String[] args) {
    log.info("---------------------");

    News news = objectMapper.readValue(JSON_NEWS, News.class);
    log.info(news.toString());

    log.info("---------------------");
    Building building = objectMapper.readValue(JSON_BUILDING, Building.class);
    log.info(building.toString());

    log.info("---------------------");
    List<User> userList = objectMapper.readValue(JSON_USERS, new TypeReference<List<User>>() {
    });
    log.info(userList.toString());

    log.info("---------------------");
    MailObject mailObject = objectMapper.readValue(JSON_EMAILS, MailObject.class);
    log.info(mailObject.toString());
    log.info("---------------------");


  }
}
