package com.example.objectmapper.model.building;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Owner {
  @JsonProperty("name")
  private String name;
  @JsonProperty("nickname")
  private String nickname;
  @JsonProperty("published")
  private Boolean published;
  @JsonProperty("birthday")
  private Date birthday;
}
