package com.example.objectmapper;

public class Jsons {
  public static final String JSON_NEWS = "{\n" +
    "    \"title\": \"omg\",\n" +
    "    \"description\": \"omg\",\n" +
    "    \"published\": false\n" +
    "}";

  public static final String JSON_BUILDING = "{\n" +
    "    \"houseName\": \"omg\",\n" +
    "    \"address\": {\n" +
    "        \"city\": \"city\",\n" +
    "        \"country\": \"country\",\n" +
    "        \"street\": \"street\"\n" +
    "    },\n" +
    "    \"owner\": {\n" +
    "        \"name\": \"name\",\n" +
    "        \"nickname\": \"nn\",\n" +
    "        \"published\": true,\n" +
    "        \"birthday\": 1653120569040\n" +
    "    },\n" +
    "    \"neighbors\": [\n" +
    "        \"neighbor1\",\n" +
    "        \"neighbor2\"\n" +
    "    ]\n" +
    "}";

  public static final String JSON_USERS = "[\n" +
    "    {\n" +
    "        \"firstName\": \"firstName\",\n" +
    "        \"secondName\": \"secondName\",\n" +
    "        \"creationDate\": 1653120825516\n" +
    "    },\n" +
    "    {\n" +
    "        \"firstName\": \"kirill\",\n" +
    "        \"secondName\": \"varchev\",\n" +
    "        \"creationDate\": 1653120825000\n" +
    "    },\n" +
    "    {\n" +
    "        \"firstName\": \"andrey\",\n" +
    "        \"secondName\": \"bugaev\",\n" +
    "        \"creationDate\": 1653120825000\n" +
    "    }\n" +
    "]";

  public static final String JSON_EMAILS = "{\n" +
    "  \"mailGroups\": [\n" +
    "    {\n" +
    "      \"name\": \"important\",\n" +
    "      \"hosts\": [\n" +
    "        {\"host\": \"31.131.254.218\"}\n" +
    "      ],\n" +
    "      \"mailTypes\": [\n" +
    "        \"email_confirmation_new_v2\",\n" +
    "        \"success_registration\",\n" +
    "        \"registration_continue\",\n" +
    "        \"change_password\",\n" +
    "        \"mobile_device_remind_password\",\n" +
    "        \"remind_password_v2\",\n" +
    "        \"change_email\"\n" +
    "      ]\n" +
    "    },\n" +
    "    {\n" +
    "      \"name\": \"all\",\n" +
    "      \"hosts\": [\n" +
    "        {\"host\": \"31.131.254.218\", \"weight\": 10},\n" +
    "        {\"host\": \"188.124.55.80\", \"weight\": 10}\n" +
    "      ],\n" +
    "      \"mailTypes\": [\n" +
    "        \"w1_purchase_notification\",\n" +
    "        \"order_tickets\",\n" +
    "        \"w1_payment_notification\",\n" +
    "        \"w1_transfer_notification\",\n" +
    "        \"bilet_info\",\n" +
    "        \"subscription_payment_error\",\n" +
    "        \"subscription_registration_error\",\n" +
    "        \"subscription_status\",\n" +
    "        \"api_backcall\",\n" +
    "        \"vip_callme\",\n" +
    "        \"feedback_form_vip\"\n" +
    "      ]\n" +
    "    }\n" +
    "  ],\n" +
    "  \"commonRelayParams\": {\n" +
    "    \"banPeriod\": \"10m\",\n" +
    "    \"failsBeforeBan\": 100,\n" +
    "    \"failsCalculationPeriod\": \"2m\",\n" +
    "    \"timeout\": \"8s\",\n" +
    "    \"connectionTimeout\": \"2s\"\n" +
    "  }\n" +
    "}";
}
