package com.example.objectmapper.model.building;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Building {
  @JsonProperty("houseName")
  private String houseName;
  @JsonProperty("address")
  private Address address;
  @JsonProperty("owner")
  private Owner owner;
  @JsonProperty("neighbors")
  private List<String> neighbors;

}
