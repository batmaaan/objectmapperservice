package com.example.objectmapper.model.mail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonRelayParams {
  @JsonProperty("banPeriod")
  private String banPeriod;
  @JsonProperty("failsBeforeBan")
  private int failsBeforeBan;
  @JsonProperty("failsCalculationPeriod")
  private String failsCalculationPeriod;
  @JsonProperty("timeout")
  private String timeout;
  @JsonProperty("connectionTimeout")
  private String connectionTimeout;
}
