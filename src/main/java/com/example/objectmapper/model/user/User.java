package com.example.objectmapper.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
  @JsonProperty("firstName")
  private String firstName;
  @JsonProperty("secondName")
  private String secondName;
  @JsonProperty("creationDate")
  private Date creationDate;
}
